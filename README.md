# Project_PCSC

This is the course project of Math-458 at EPFL in Switzerland. The objective of this project is to implement the numerical methods, e.g bisection, chord, newton & raphson, fixed point method and aitken acceleration for the solution of nonlinear scalar equation. For nonlinear system, the Newton & Raphson method is used.

The main structure is as follows. Not all the files are explicitly presented.

```
├── eigen                   # external library: eigen
├── googletest              # googletest folder
├── Dxygen                  # Doxygen folder
├── src                     # src files
│   ├── readfile.cpp        
│   ├── readfile.h          # reading input head file
│   ├── implement.cpp          
│   ├── implement.h         # implementation head file of preprocessing and methods 
│   ├── function.cpp         
│   ├── function.h          # definition of functions
│   ├── main.cpp           
│   ├── input.txt           # input txt file of custom function
│   ├── input.csv           # input csv file of custom function
│   ├── output.txt          # output txt file
│   ├── CMakeList.txt       # CMakeList file
│   └── ...                 # Other files
├── test                    # Test files
│   ├── ...                 # same files as the first 6 files in src
│   ├── testset             # test folder
│   ├── test.cpp         
│   ├── test.h              # return the function value of computed root
│   ├── unit_test.cpp       # Unit tests
│   ├── CMakeList.txt       # CMakeList file
│   └── ...                 # Other files
├── CMakeList.txt           # CMakeList file
├── report.pdf              # pcsc report
└── ...
```

## Construction of this project

To arrange this project, we start by creating readfile.h and readfile.cpp files that perform the function of reading txt and csv files, as well as implementing polymorphism. The class is defined in the head files, and the class's functions are implemented in the cpp file.

After that, we define function.h and function.cpp with the objective of implementing the five methods for computing the roots with less code repetition. That's how the functions are defined.

Following that, we construct the Implement.h and Implement.cpp files to implement the root-finding iterative methods. The header files include seven classes, one for pre-processing the input of methods and others for implementing the numerical methods.

Later, in order to test the test suite, we build test.h and test.cpp to return the average error between the function value and 0.

For the test, a test fixture is defined to do six tests on the five methods.

Finally, the main.cpp file is used to execute the implementation, depending on the custom function the users want to use. The computed roots will be written into output.txt.

## Usage of important cpp files
 - readfile.cpp: Realize the file reading class, following the principle of inheritance, encapsulation, and polymorphism.
 - function.cpp: Construct functions that implement various methods and preprocessing, read files and output files and etc.
 - implement.cpp: Realize the method class for calculating the roots and store related variables.
 - test.cpp : Construct the functions that calculate the errors of the function at the roots derived by the methods.
 - main.cpp : Have a deeper look at the working flow of this project from reading parameters to preprocessing, root computation and output.
 More details regarding the class are listed in the Dxygen folder.

## Contact
Any issue please contact the authors:
 - Jingran Su : jingran.su@epfl.ch
 - Jun Qing : jun.qing@epfl.ch
