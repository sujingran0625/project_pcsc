#include <iostream>
#include <vector>
#include "function.h"
#include "implement.h"
#include "readfile.h"
using namespace std;


int main() // solve the zero point for polynomial function with different implementations
{
	// example path loading (please use the full path to the test files)
	//string path_poly = "test/test_poly1.txt";
	//string path_poly_csv = "test/test01.csv";
	//string path_trigonometric = "test/test_tri1.txt";
	//string path_exponential = "test/test_exp.txt";
	//string path_matrix = "test/matrix1.txt";

    // It is possible to define the function(s) in input.txt or input.csv (full path)
	string path = "input.txt";

	// example reading part
	//vector<float> v_read = Read_File_txt(path_trigonometric); // choose between reading a txt or csv file, and choose the path you want to read(except for path_matrix)
	//vector<float> v_read = Read_File_csv(path_poly_csv); // example of csv reading
	//Eigen::MatrixXf m_read = Read_matrix_txt(path_matrix); // used to read the path_matrix

    // reading input from input.txt can be defined here

	// implementation part
	//Imple_chord r1(v_read);
	//Imple_bisection r1(v_read);
	//Imple_newton r1(v_read);
	//Imple_fixed_point r1(v_read);
	//Imple_fixed_point_aitken r1(v_read);
	//Equations r1(m_read); // only used for the path_matrix reading

    // implementation should be defined here
	
	// execution part
	//r1.func();
	//r1.get_output();


	//float b = equations_test(path_matrix); google test
	
	return 0;
}
