#include "test.h"
#include <gtest/gtest.h>

namespace my {
    namespace project {
namespace {
    class FunctionTest : public ::testing::Test {
    protected:

        FunctionTest() {
        }

        ~FunctionTest() override {
            // No clean-up work to do here
        }

        void SetUp() override {
        }

        void TearDown() override {
        }

        // Class members declared here can be used by all tests in the test suite
        // for Function.
        string path_poly1 = "/home/jisu/myfiles/Programmation/project_pcsc/test/testset/test_poly1.txt";
        string path_poly2 = "/home/jisu/myfiles/Programmation/project_pcsc/test/testset/test_poly2.txt";
        string path_trigonometric1 = "/home/jisu/myfiles/Programmation/project_pcsc/test/testset/test_tri1.txt";
        string path_trigonometric2 = "/home/jisu/myfiles/Programmation/project_pcsc/test/testset/test_tri2.txt";
        string path_exponential = "/home/jisu/myfiles/Programmation/project_pcsc/test/testset/test_exp.txt";
        string path_matrix1 = "/home/jisu/myfiles/Programmation/project_pcsc/test/testset/matrix1.txt";
        string path_matrix2 = "/home/jisu/myfiles/Programmation/project_pcsc/test/testset/matrix2.txt";
        string path_matrix3 = "/home/jisu/myfiles/Programmation/project_pcsc/test/testset/matrix3.txt";
    };
// Test for Bisection method
    TEST_F(FunctionTest, Bisection) {
        EXPECT_NEAR(0, float (bisection_test(path_poly1)), 0.001);
        EXPECT_NEAR(0, float (bisection_test(path_poly2)), 0.001);
        EXPECT_NEAR(0, float (bisection_test(path_trigonometric1)), 0.001);
        EXPECT_NEAR(0, float (bisection_test(path_trigonometric2)), 0.001);
        EXPECT_NEAR(0, float (bisection_test(path_exponential)), 0.001);
    }

// Test for Chord method
    TEST_F(FunctionTest, Chord) {
        EXPECT_NEAR(0, float (chord_test(path_poly1)), 0.001);
        EXPECT_NEAR(0, float (chord_test(path_poly2)), 0.001);
        EXPECT_NEAR(0, float (chord_test(path_trigonometric1)), 0.001);
        EXPECT_NEAR(0, float (chord_test(path_trigonometric2)), 0.001);
        EXPECT_NEAR(0, float (chord_test(path_exponential)), 0.001);
    }
// Test for Newton Raphson method
    TEST_F(FunctionTest, Newton) {
        EXPECT_NEAR(0, float (newton_test(path_poly1)), 0.02);
        EXPECT_NEAR(0, float (newton_test(path_poly2)), 0.02);
        EXPECT_NEAR(0, float (newton_test(path_trigonometric1)), 0.02);
        EXPECT_NEAR(0, float (newton_test(path_trigonometric2)), 0.02);
        EXPECT_NEAR(0, float (newton_test(path_exponential)), 0.1);
    }
// Test for Fixed point method
    TEST_F(FunctionTest, FixedPoint) {
        EXPECT_NEAR(0, float (fixed_point_test(path_poly1)), 0.02);
        EXPECT_NEAR(0, float (fixed_point_test(path_poly2)), 0.02);
        EXPECT_NEAR(0, float (fixed_point_test(path_trigonometric1)), 0.02);
        EXPECT_NEAR(0, float (fixed_point_test(path_trigonometric2)), 0.02);
        EXPECT_NEAR(0, float (fixed_point_test(path_exponential)), 0.02);
    }
//Test for Aitken acceleration of Fixed point method
    TEST_F(FunctionTest, AitkenPixedPoint) {
        EXPECT_NEAR(0, float (aitken_test(path_poly1)), 0.001);
        EXPECT_NEAR(0, float (aitken_test(path_poly2)), 0.001);
        EXPECT_NEAR(0, float (aitken_test(path_trigonometric1)), 0.001);
        EXPECT_NEAR(0, float (aitken_test(path_trigonometric2)), 0.001);
        EXPECT_NEAR(0, float (aitken_test(path_exponential)), 0.001);
    }
// Test for Newton method solved for nonlinear polynomial system
    TEST_F(FunctionTest, Matrix) {
        EXPECT_NEAR(0, float (equations_test(path_matrix1)), 0.01);
        EXPECT_NEAR(0, float (equations_test(path_matrix2)), 0.01);
        EXPECT_NEAR(0, float (equations_test(path_matrix3)), 0.01);
    }

}  // namespace
}  // namespace project
}  // namespace my

int main(int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}



